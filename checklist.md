Software Quality Checklist
==========================

General
-------

-  [x] Does the software have a descriptive name?
-  [x] Is there a short high-level description of the software?
-  [x] Is the purpose of the software clear?
-  [x] Is the targeted audience of the software clear?
-  [x] Does it (and its dependencies) use OSI approved licenses?
-  [x] Is the software under version control?
-  [ ] Is there a website for the software?
-  [ ] Does the software have a release mechanism?
-  [ ] Is the software available in packaged format or only sources?
-  [x] Are maintainer and development status clear, including contact information?
-  [x] Are the requirements listed and up to date?
-  [ ] Is the interface responsive and accessible?
-  [x] Is copyright and authorship clear?
-  [ ] Is there a contribution guide?

Documentation
-------------

-  [x] Is there an accessible getting started guide?
-  [x] Is there an accessible user guide?
-  [x] Is there a full user documentation?
-  [ ] Does the user interface link to held references?
-  [x] Are there examples, FAQs and tutorials?
-  [x] Are known issues documented?

Development
-----------

-  [ ] Is the development setup documented?
-  [ ] Is the build mechanism documented?
-  [ ] Does the build mechanism use a common single-command system (i.e. Maven)?
-  [ ] Is the software API documented?
-  [x] Are all appropriate config options externalised and documented?
-  [ ] Does the code allow internationalisation (i18n)?
-  [ ] Is the software localised (l10n)? English is mandatory.
-  [ ] Is there a test suite?
-  [ ] Is test coverage above 80%?

Interoperability
----------------

-  [x] Are file formats standard compliant and documented?
-  [ ] Is the API standard compliant?
-  [ ] Does it provide a monitoring endpoint?
-  [ ] Does it adhere to an interface style guide?
-  [ ] Does it use existing authentication systems (OAuth2/eduGain)?

Administration
--------------

-  [x] Are software requirements such as operating system, required libraries and dependencies specified including versions?
-  [ ] Are hardware requirements for CPU, RAM, HDD, Network specified?
-  [ ] Are there deployment instructions?
-  [x] Is there a comprehensive and fully documented example configuration?
-  [x] Is a start-up script provided?
-  [x] Are there troubleshooting guides?
